### 指令概览
在 Angular 中有三种类型的指令：

1. 组件 — 拥有模板的指令

2. 结构型指令 — 通过添加和移除 DOM 元素改变 DOM 布局的指令

3. 属性型指令 — 改变元素、组件或其它指令的外观和行为的指令。

**组件**是这三种指令中最常用的。 你在快速上手例子中第一次见到组件。

**结构型指令**修改视图的结构。例如，`NgFor` 和 `NgIf`。 要了解更多，参见结构型指令 guide。

**属性型指令**改变一个元素的外观或行为。例如，内置的 `NgStyle` 指令可以同时修改元素的多个样式。


创建一个简单的属性型指令


编写指令代码

```
    ng generate directive highlight
```

生成的 `src/app/highlight.directive.ts` 文件如下：

```
    import { Directive } from '@angular/core';
    
    @Directive({
      selector: '[appHighlight]'
    })
    export class HighlightDirective {
    
     constructor(el: ElementRef) {
            el.nativeElement.style.backgroundColor = 'yellow';
         }
    }
```
这里导入的 `Directive` 符号提供了 Angular 的 `@Directive` 装饰器。

`@Directive` 装饰器的配置属性中指定了该指令的 `CSS` 属性型选择器 `[appHighlight]`

这里的方括号`([])`表示它的属性型选择器。 Angular 会在模板中定位每个名叫 `appHighlight` 的元素，并且为这些元素加上本指令的逻辑。

正因如此，这类指令被称为 属性选择器 。

紧跟在 `@Directive` 元数据之后的就是该指令的控制器类，名叫 `HighlightDirective`，它包含了该指令的逻辑（目前为空逻辑）。

然后导出 `HighlightDirective`，以便它能在别处访问到。

使用属性型指令

```
    <p appHighlight>Highlight me!</p>
```

响应用户引发的事件

先把 `HostListener` 加进导入列表中。

然后使用 `HostListene` 装饰器添加两个事件处理器，它们会在鼠标进入或离开时进行响应。

```
   import { Directive, ElementRef, HostListener } from '@angular/core';
    
   @Directive({
     selector: '[appHighlight]'
   })
   export class HighlightDirective {
     constructor(private el: ElementRef) { }
    
     @HostListener('mouseenter') onMouseEnter() {
       this.highlight('yellow');
     }
    
     @HostListener('mouseleave') onMouseLeave() {
       this.highlight(null);
     }
    
     private highlight(color: string) {
       this.el.nativeElement.style.backgroundColor = color;
     }
```

使用 `@Input` 数据绑定向指令传递值

先从 `@angular/core` 中导入 `Input`。

```
    import { Directive, ElementRef, HostListener, Input } from '@angular/core';
```

然后把 `highlightColor` 属性添加到指令类中，就像这样：

```
@Input() highlightColor: string;
```

绑定到 `@Input` 属性

```
   <p [highlightColor]="color">Highlight me!</p>
```

把 `color` 属性添加到 `AppComponent`中：

```
    export class AppComponent {
      color = 'yellow';
    }
```

现在，修改 onMouseEnter() 方法来使用它。

```
    @HostListener('mouseenter') onMouseEnter() {
      this.highlight(this.highlightColor || 'red');
    }
```

### 结构型指令


在这个例子中，你将学到星号(*)这个简写方法，而这个字符串是一个微语法，而不是通常的模板表达式。 

Angular 会解开这个语法糖，变成一个 <ng-template> 标记，包裹着宿主元素及其子元素。 每个结构型指令都可以用这个模板做点不同的事情。

三个常用的内置结构型指令 —— `NgIf、NgFor`和`NgSwitch`...。 



在本章中，你将看到指令同时具有两种拼写形式大驼峰 UpperCamelCase 和小驼峰 lowerCamelCase，

比如你已经看过的 NgIf 和 ngIf。 这里的原因在于，NgIf 引用的是指令的类名，而 ngIf 引用的是指令的属性名*。

指令的类名拼写成大驼峰形式（NgIf），而它的属性名则拼写成小驼峰形式（ngIf）。 

本章会在谈论指令的属性和工作原理时引用指令的类名，在描述如何在 HTML 模板中把该指令应用到元素时，引用指令的属性名。

`*ngFor` 内幕

```
    <div *ngFor="let hero of heroes; let i=index; let odd=odd; trackBy: trackById" [class.odd]="odd">
      ({{i}}) {{hero.name}}
    </div>
```

每个宿主元素上只能有一个结构型指令

有时你会希望只有当特定的条件为真时才重复渲染一个 HTML 块。 

你可能试过把 `*ngFor` 和 `*ngIf` 放在同一个宿主元素上，但 Angular 不允许。这是因为你在一个元素上只能放一个结构型指令。


 `NgSwitch` 内幕

```
   <div [ngSwitch]="hero?.emotion">
     <app-happy-hero    *ngSwitchCase="'happy'"    [hero]="hero"></app-happy-hero>
     <app-sad-hero      *ngSwitchCase="'sad'"      [hero]="hero"></app-sad-hero>
     <app-confused-hero *ngSwitchCase="'app-confused'" [hero]="hero"></app-confused-hero>
     <app-unknown-hero  *ngSwitchDefault           [hero]="hero"></app-unknown-hero>
   </div>
```

`<ng-template>`指令

`<ng-template>`是一个 Angular 元素，用来渲染 HTML。 它永远不会直接显示出来。 

事实上，在渲染视图之前，Angular 会把 `<ng-template>` 及其内容替换为一个注释。

如果没有使用结构型指令，而仅仅把一些别的元素包装进 `<ng-template>` 中，那些元素就是不可见的。
 
 在下面的这个短语"Hip! Hip! Hooray!"中，中间的这个 "Hip!"（欢呼声） 就是如此。

```
    <p>Hip!</p>
    <ng-template>
      <p>Hip!</p>
    </ng-template>
    <p>Hooray!</p>
```
![](../assets/template-rendering.png)

`<ng-container>` 的救赎

Angular 的 `<ng-container>` 是一个分组元素，但它不会污染样式或元素布局，因为 Angular 压根不会把它放进 DOM 中。

现在用 `<ng-container>` 来根据条件排除选择框中的某个 `<option>`。

```
    <div>
      Pick your favorite hero
      (<label><input type="checkbox" checked (change)="showSad = !showSad">show sad</label>)
    </div>
    <select [(ngModel)]="hero">
      <ng-container *ngFor="let h of heroes">
        <ng-container *ngIf="showSad || h.emotion !== 'sad'">
          <option [ngValue]="h">{{h.name}} ({{h.emotion}})</option>
        </ng-container>
      </ng-container>
    </select>
```

写一个结构型指令

在本节中，你会写一个名叫 `UnlessDirective` 的结构型指令，它是 `NgIf` 的反义词。 

`NgIf` 在条件为 `true` 的时候显示模板内容，而 `UnlessDirective` 则会在条件为 `false` 时显示模板内容。

创建指令很像创建组件。

* 导入 `Directive` 装饰器（而不再是 Component）。

* 导入符号 `Input、TemplateRef` 和 `ViewContainerRef`，你在任何结构型指令中都会需要它们。

* 给指令类添加装饰器。

* 设置 CSS 属性选择器 ，以便在模板中标识出这个指令该应用于哪个元素。

`TemplateRef `和 `ViewContainerRef`

你可以使用`TemplateRef`取得 `<ng-template>` 的内容，并通过`ViewContainerRef`来访问这个视图容器。

你可以把它们都注入到指令的构造函数中，作为该类的私有属性。

```
    constructor(
      private templateRef: TemplateRef<any>,
      private viewContainer: ViewContainerRef) { }
```

`appUnless` 属性

该指令的使用者会把一个` true/false `条件绑定到 `[appUnless]` 属性上。 

该指令需要一个带有 `@Input` 的 `appUnless` 属性。

```
    @Input() set appUnless(condition: boolean) {
      if (!condition && !this.hasView) {
        this.viewContainer.createEmbeddedView(this.templateRef);
        this.hasView = true;
      } else if (condition && this.hasView) {
        this.viewContainer.clear();
        this.hasView = false;
      }
    }
```

一旦该值的条件发生了变化，Angular 就会去设置 appUnless 属性。因为不能用 appUnless 属性，所以你要为它定义一个设置器（setter）。

* 如果条件为假，并且以前尚未创建过该视图，就告诉视图容器（ViewContainer）根据模板创建一个内嵌视图。

* 如果条件为真，并且视图已经显示出来了，就会清除该容器，并销毁该视图。

```
import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

/**
 * Add the template content to the DOM unless the condition is true.
 */
@Directive({ selector: '[appUnless]'})
export class UnlessDirective {
  private hasView = false;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) { }

  @Input() set appUnless(condition: boolean) {
    if (!condition && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (condition && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
}
```