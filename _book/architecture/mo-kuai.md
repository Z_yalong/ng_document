`Angular` 应用是模块化的，它拥有自己的模块化系统，称作 `NgModule`。 一个 `NgModule` 就是一个容器，用于存放一些内聚的代码块，这些代码块专注于某个应用领域、某个工作流或一组紧密相关的功能。 它可以包含一些组件、服务提供商或其它代码文件，其作用域由包含它们的 `NgModule` 定义。 它还可以导入一些由其它模块中导出的功能，并导出一些指定的功能供其它 `NgModule` 使用。

### @NgModule 元数据

`NgModule` 是一个带有 `@NgModule` 装饰器的类。`@NgModule` 装饰器是一个函数，它接受一个元数据对象，该对象的属性用来描述这个模块。其中最重要的属性如下。

* `declarations`（可声明对象表） —— 那些属于本 NgModule 的组件、指令、管道。

* `exports`（导出表） —— 那些能在其它模块的组件模板中使用的可声明对象的子集。

* `imports`（导入表） —— 那些导出了本模块中的组件模板所需的类的其它模块。

* `providers` —— 本模块向全局服务中贡献的那些服务的创建器。 这些服务能被本应用中的任何部分使用。（你也可以在组件级别指定服务提供商，这通常是首选方式。）

* `bootstrap` —— 应用的主视图，称为根组件。它是应用中所有其它视图的宿主。只有根模块才应该设置这个 `bootstrap` 属性。

下面是一个简单的根 `NgModule` 定义：



```
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
@NgModule({
  imports:      [ BrowserModule ],
  providers:    [ Logger ],
  declarations: [ AppComponent ],
  exports:      [ ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
```

### NgModule 和组件

`NgModule` 为其中的组件提供了一个编译上下文环境。根模块总会有一个根组件，并在引导期间创建它。 但是，任何模块都能包含任意数量的其它组件，这些组件可以通过路由器加载，也可以通过模板创建。那些属于这个 `NgModule` 的组件会共享同一个编译上下文环境。

![](../assets/compilation-context.png)

组件及其模板共同定义视图。组件还可以包含视图层次结构，它能让你定义任意复杂的屏幕区域，可以将其作为一个整体进行创建、修改和销毁。 一个视图层次结构中可以混合使用由不同 `NgModule` 中的组件定义的视图。 

![](../assets/view-hierarchy.png)

### Angular 自带的库

`Angular` 自带了一组 `JavaScript` 模块，你可以把它们看成库模块。每个 `Angular` 库的名称都带有 `@angular` 前缀。 使用 `npm` 包管理器安装它们，并使用 `JavaScript` 的 `import` 语句导入其中的各个部分。

```
  import { BrowserModule } from '@angular/platform-browser';
```

上面这个简单的根模块范例中，应用的根模块需要来自 `BrowserModule` 中的素材。要访问这些素材，就要把它加入 `@NgModule` 元数据的 `imports` 中，代码如下：

`imports:      [ BrowserModule ],`
