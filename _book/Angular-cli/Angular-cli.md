
###安装前提条件

在使用 Angular CLI 之前，你必须确保系统中 Node.js 的版本高于 6.9.0 且 npm 的版本高于 3.0.0。

若你尚未安装 Node.js，你可以访问 Node.js 官网，然后根据你所用的操作系统选择对应的安装方式。

若你本机已经安装 Node.js 和 npm，你可以通过运行以下命令，确认一下当前环境信息：

` $ node - v # 显示当前Node.js的版本`

` $ npm -v # 显示当前npm的版本`

当你本机 Node.js 环境确认无误后，你可以在命令行使用 npm 安装 TypeScript：

` $ npm install -g typescript # 安装最新的TypeScript稳定版`

###安装 Angular CLI

* <p>安装 Angular CLI (可选)</p>

`npm install -g @angular/cli`

* <p>检测 Angular CLI 是否安装成功</p>

`ng --version`

* <p>创建新的项目</p>

`ng new xxxxxxxxx // xxxxxxxxx you project name`

![创建中，npm也在下载相关的包，耐心等待](../assets/3020614-6192f68e63bbc7cc.png)
上图就是创建的过程，此时在创建中，npm也在下载相关的包，耐心等待；从控制台中可以看到创建的内容； 

* <p>启动本地服务器</p>

    `cd xxxxxxxxx`           
    `ng serve`

浏览器输入 http://localhost:4200/ 就可以看到app works！ 
