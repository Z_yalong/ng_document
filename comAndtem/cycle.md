### 生命周期钩子

 每个组件都有一个被 Angular 管理的生命周期。
 
 Angular 创建它，渲染它，创建并渲染它的子组件，在它被绑定的属性发生变化时检查它，并在它从 DOM 中被移除前销毁它。
 
 Angular 提供了生命周期钩子，把这些关键生命时刻暴露出来，赋予你在它们发生时采取行动的能力。
 
 除了那些组件内容和视图相关的钩子外,指令有相同生命周期钩子。
 
   <table>
      <thead>
         <tr>
            <th>钩子</th>
            <th>用途及时机</th>
         </tr>   
      </thead>
      <tbody>
         <tr>
            <td>ngOnChanges()</td>
            <td> 当 Angular（重新）设置数据绑定输入属性时响应。 该方法接受当前和上一属性值的 SimpleChanges 对象 <br/>
                 当被绑定的输入属性的值发生变化时调用，首次调用一定会发生在 ngOnInit() 之前
            </td>
         </tr>  
         <tr>
            <td>ngOnInit()</td>
            <td> 在 Angular 第一次显示数据绑定和设置指令/组件的输入属性之后，初始化指令/组件。<br/>
                 在第一轮 ngOnChanges() 完成之后调用，只调用一次。
            </td>
         </tr>  
         <tr>
            <td>ngDoCheck()</td>
            <td> 当 Angular（重新）设置数据绑定输入属性时响应。 该方法接受当前和上一属性值的 SimpleChanges 对象 <br/>
                 当被绑定的输入属性的值发生变化时调用，首次调用一定会发生在 ngOnInit() 之前
            </td>
         </tr>  
         <tr>
            <td>ngAfterContentInit()</td>
            <td> 当把内容投影进组件之后调用。<br/>
                 第一次 ngDoCheck() 之后调用，只调用一次。
            </td>
         </tr>  
         <tr>
            <td>ngAfterContentChecked()</td>
            <td> 每次完成被投影组件内容的变更检测之后调用。<br/>
                 ngAfterContentInit() 和每次 ngDoCheck() 之后调用
            </td>
         </tr>     
         <tr>
            <td>ngAfterViewInit()</td>
            <td> 初始化完组件视图及其子视图之后调用。<br/>
                第一次 ngAfterContentChecked() 之后调用，只调用一次。
            </td>
         </tr>       
         <tr>
            <td>ngAfterViewChecked()</td>
            <td> 每次做完组件视图和子视图的变更检测之后调用。。<br/>
                ngAfterViewInit() 和每次 ngAfterContentChecked() 之后调用。
            </td>
         </tr>       
         <tr>
            <td>ngOnDestroy()</td>
            <td> 当 Angular 每次销毁指令/组件之前调用并清扫。 在这儿反订阅可观察对象和分离事件处理器，以防内存泄漏。<br/>
                 在 Angular 销毁指令/组件之前调用。
            </td>
         </tr>          
      </tbody>
      
   </table>
   
组件所有钩子执行顺序
   
   ![](../assets/peek-a-boo.png)
   
 日志信息的日志和所规定的钩子调用顺序是一致的： `OnChanges、OnInit、DoCheck (3x)、AfterContentInit、AfterContentChecked (3x)、 AfterViewInit、AfterViewChecked (3x)和 OnDestroy ` 
 
 构造函数(`constructor`)中除了使用简单的值对局部变量进行初始化之外，什么都不应该做。
 
 