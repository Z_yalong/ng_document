### 组件样式

实现方式之一，是在组件的元数据中设置 `styles` 属性。 `styles` 属性可以接受一个包含 `CSS` 代码的字符串数组。 通常你只给它一个字符串就行了，如同下例：

 ![](../assets/css1.png)
 
 在 `@Component` 的元数据中指定的样式只会对该组件的模板生效。
 
 这种范围限制就是所谓的样式模块化特性
 
 * 可以使用对每个组件最有意义的 CSS 类名和选择器。
 
 * 类名和选择器是仅属于组件内部的，它不会和应用中其它地方的类名和选择器出现冲突。
 
 * 组件的样式不会因为别的地方修改了样式而被意外改变。
 
 * 你可以让每个组件的 CSS 代码和它的 TypeScript、HTML 代码放在一起，这将促成清爽整洁的项目结构。
 
 * 将来你可以修改或移除组件的 CSS 代码，而不用遍历整个应用来看它有没有被别处用到，只要看看当前组件就可以了。
 
 模板内联样式 
 
 你也可以在组件的 HTML 模板中嵌入 `<style>` 标签
 
 ```
    @Component({
      selector: 'app-hero-controls',
      template: `
        <style>
          button {
            background-color: white;
            border: 1px solid #777;
          }
        </style>
        <h3>Controls</h3>
        <button (click)="activate()">Activate</button>
      `
    })
```

模板中的 link 标签

```
    @Component({
      selector: 'app-hero-team',
      template: `
        <!-- We must use a relative URL so that the AOT compiler can find the stylesheet -->
        <link rel="stylesheet" href="../assets/hero-team.component.css">
        <h3>Team</h3>
        <ul>
          <li *ngFor="let member of hero.team">
            {{member}}
          </li>
        </ul>`
    })

```

CSS @imports 语法

你还可以利用标准的 CSS @import 规则来把其它 CSS 文件导入到 CSS 文件中。

在这种情况下，URL 是相对于你正在导入的 CSS 文件的


```
    @import './hero-details-box.css';
```

非 CSS 样式文件

使用 CLI 进行构建，那么你可以用 sass、less 或 stylus 来编写样式，并使用相应的扩展名（.scss、.less、.styl）把它们指定到 @Component.styleUrls 元数据中。例子如下：

```
    @Component({
      selector: 'app-root',
      templateUrl: './app.component.html',
      styleUrls: ['./app.component.scss']
    })
```

