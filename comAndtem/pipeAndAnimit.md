### 管道 （|）


在这个插值表达式中，你让组件的 `birthday` 值通过管道操作符( | date )流动到 右侧的`Date` 管道函数中。所有管道都会用这种方式工作。

* 内置的管道

Angular 内置了一些管道，比如 `DatePipe、UpperCasePipe、LowerCasePipe、CurrencyPipe 和 PercentPipe`。 它们全都可以直接用在任何模板中。

要学习更多内置管道的知识，[参见API 参考手册](https://angular.cn/api?type=pipe)，并用“pipe”为关键词对结果进行过滤

* 对管道进行参数化

管道可能接受任何数量的可选参数来对它的输出进行微调。 可以在管道名后面添加一个冒号( : )再跟一个参数值，来为管道添加参数(比如 currency:'EUR')。

* 链式管道  
* 自定义管道

你还可以写自己的自定义管道。 下面就是一个名叫 `ExponentialStrengthPipe` 的管道，它可以放大英雄的能力：

```
    import { Pipe, PipeTransform } from '@angular/core';
    /*
    * Raise the value exponentially
    * Takes an exponent argument that defaults to 1.
    * Usage:
    *   value | exponentialStrength:exponent
    *   formats to: 1024
    */
    @Pipe({name: 'exponentialStrength'})
    export class ExponentialStrengthPipe implements PipeTransform {
        transform(value: number, exponent: string): number {
            let exp = parseFloat(exponent);
            return Math.pow(value, isNaN(exp) ? 1 : exp);
        }
    }
```   
在这个管道的定义中体现了几个关键点：

* 管道是一个带有“管道元数据(pipe metadata)”装饰器的类。

* 这个管道类实现了 `PipeTransform` 接口的 `transform `方法，该方法接受一个输入值和一些可选参数，并返回转换后的值。

* 当每个输入值被传给 `transform` 方法时，还会带上另一个参数，比如你这个管道就有一个 `exponent`(放大指数) 参数。

* 可以通过 `@Pipe` 装饰器来告诉 Angular：这是一个管道。该装饰器是从 Angular 的 `core` 库中引入的。

* 这个` @Pipe` 装饰器允许你定义管道的名字，这个名字会被用在模板表达式中。它必须是一个有效的 JavaScript 标识符。 比如，你这个管道的名字是 `exponentialStrength`。

### 动画

动画是现代 Web 应用设计中一个很重要的方面。好的用户界面要能在不同的状态之间更平滑的转场。如果需要，还可以用适当的动画来吸引注意力。 设计良好的动画不但会让 UI 更有趣，还会让它更容易使用。

* 准备工作

在往应用中添加动画之前，你要首先在应用的根模块中引入一些与动画有关的模块和函数。

```
    import { BrowserModule } from '@angular/platform-browser';
    import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

    @NgModule({
    imports: [ BrowserModule, BrowserAnimationsModule ],
    // ... more stuff ...
    })
    export class AppModule { }
```

动画会被定义在 @Component 元数据中。

```
    @Component({
    selector: 'app-city-accountset',
    templateUrl: './city-accountset.component.html',
    styleUrls: ['./city-accountset.component.css'],
    animations: [fadeIn]
    })
```
动画组件 `hero-list-basic.component.ts`

```
    import {
    trigger, // 动画封装触发，外部的触发器
    state, // 转场状态控制
    style, // 用来书写基本的样式
    transition, // 用来实现css3的 transition
    animate, // 用来实现css3的animations
    keyframes // 用来实现css3 keyframes的
    } from '@angular/animations';


        
    export const navantimate = trigger('navantimate', [
    state('show', style({  left: '0px' })), // state 具体定义了每个状态的最终样式。
    state('hide', style({ left: '-200px' })), // 具体定义了每个状态的最终样式。
    transition('show => hide', [ //  定义在状态之间的各种转场了
        animate(300)
    ]),
    transition('hide => show', [ // 定义在状态之间的各种转场了
        animate(300)
    ]),
    ]);
```

有时希望一些样式只在动画期间生效，但在结束后并不保留它们。这时可以把这些样式内联在 `transition` 中进行定义。 在这个例子中，该元素会立刻获得一组样式，然后动态转场到下一个状态。当转场结束时，这些样式并不会被保留，因为它们并没有被定义在 `state` 中。

```
    transition('inactive => active', [
    style({
        backgroundColor: '#cfd8dc',
        transform: 'scale(1.3)'
    }),
    animate('80ms ease-in', style({
        backgroundColor: '#eee',
        transform: 'scale(1)'
    }))
    ]),
```

*(通配符)状态

*(通配符)状态匹配任何动画状态。当定义那些不需要管当前处于什么状态的样式及转场时，这很有用。比如

* 当该元素的状态从 active 变成任何其它状态时，active => * 转场都会生效。

* 当在任意两个状态之间切换时，* => * 转场都会生效。

* 自动属性值计算  有时候，你在开始运行之前都无法知道某个样式属性的值。比如，元素的宽度和高度往往依赖于它们的内容和屏幕的尺寸，就可以用一个特殊的 * 属性值来处理这种情况。该属性的值将会在运行期被计算出来，然后插入到这个动画中。

动画时间线

```
0.2s 100ms ease-out  // 持续时间 延迟 缓动函数
```

基于关键帧(Keyframes)的多阶段动画 

通过定义动画的关键帧，可以把两组样式之间的简单转场，升级成一种更复杂的动画，它会在转场期间经历一个或多个中间样式。

每个关键帧都可以被指定一个偏移量，用来定义该关键帧将被用在动画期间的哪个时间点。偏移量是一个介于 0(表示动画起点)和 1(表示动画终点)之间的数组。

```
    animations: [
    trigger('flyInOut', [
        state('in', style({transform: 'translateX(0)'})),
        transition('void => *', [
        animate(300, keyframes([
            style({opacity: 0, transform: 'translateX(-100%)', offset: 0}),
            style({opacity: 1, transform: 'translateX(15px)',  offset: 0.3}),
            style({opacity: 1, transform: 'translateX(0)',     offset: 1.0})
        ]))
        ]),
        transition('* => void', [
        animate(300, keyframes([
            style({opacity: 1, transform: 'translateX(0)',     offset: 0}),
            style({opacity: 1, transform: 'translateX(-15px)', offset: 0.7}),
            style({opacity: 0, transform: 'translateX(100%)',  offset: 1.0})
        ]))
        ])
    ])
    ]
```

并行动画组(Group)

你已经知道该如何在同一时间段进行多个样式的动画了：只要把它们都放进同一个 style() 定义中就行了！

但你也可能会希望为同时发生的几个动画配置不同的时间线。比如，同时对两个 CSS 属性做动画，但又得为它们定义不同的缓动函数。

这种情况下就可以用动画组来解决了。在这个例子中，同时在进场和离场时使用了组，以便能让它们使用两种不同的时间线配置。 它们被同时应用到同一个元素上，但又彼此独立运行：

```
    animations: [
    trigger('flyInOut', [
        state('in', style({width: 120, transform: 'translateX(0)', opacity: 1})),
        transition('void => *', [
        style({width: 10, transform: 'translateX(50px)', opacity: 0}),
        group([
            animate('0.3s 0.1s ease', style({
            transform: 'translateX(0)',
            width: 120
            })),
            animate('0.3s ease', style({
            opacity: 1
            }))
        ])
        ]),
        transition('* => void', [
        group([
            animate('0.3s ease', style({
            transform: 'translateX(50px)',
            width: 10
            })),
            animate('0.3s 0.2s ease', style({
            opacity: 0
            }))
        ])
        ])
    ])
    ]
```