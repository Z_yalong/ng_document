*   引入 http

![](../assets/http.png)

*   依赖注入


![](../assets/http2.png)


*  请求头的设置 以及页面loading加载

```
    getRequestOptionsArgs() {
        let header = new Headers();
        header.append('X-Requested-With', 'XMLHttpRequest');
        header.append('Authorization', `Bearer ${this.taken()}`);
        let option: RequestOptionsArgs = { headers: header };
        return option;
  }
```

* get post 重写 

```
        /**
    * http get重写
    */
    async get(url: string, option?: RequestOptionsArgs) {
        await this.validtimeout();
        if (!option) option = { params: { ts: new Date().getTime() } };
        option.withCredentials = true;
        Object.assign(option, this.getRequestOptionsArgs());
        this.block.blockUI(); 
        const result = await this.http.get(this.domain + url, option).toPromise().then(res => {
        return this.HandSuccessResult(res);
        }).catch(async res => {
        await this.HandErrorResult(res);
        });
        this.lastdate = new Date();
        return result;
    }

    /**
    * http post重写
    */
    async post(url: string, body?: any, option?: RequestOptionsArgs) {
        await this.validtimeout();
        if (!option) option = {};

        option.withCredentials = true;
        Object.assign(option, this.getRequestOptionsArgs());

        if (body && Object.prototype.toString.call(body) === "[object Object]") {
        body = format_object(body);
        }
        this.block.blockUI();
        const result = await this.http.post(this.domain + url, body, option).toPromise().then(res => {
        return this.HandSuccessResult(res);
        }).catch(async res => {
        await this.HandErrorResult(res);
        });
        this.lastdate = new Date();
        return result;
    }
``` 

* 处理 正确和错误请求

```
        /**
    * 处理http正确返回结果
    */
    HandSuccessResult(res: Response) {
        let httpresult = res.json();
        if (httpresult.result) {
        this.block.UnBlockUI();
        if (httpresult.data) {
            return httpresult.data;
        } else {
            return true;
        }
        }
        else {
        // this.mydialog.openMessage(httpresult.message, "操作失败", "error");
        throw res;
        }
    }

    /**
    * 处理http错误正确返回结果
    */
    async HandErrorResult(res: Response) {
        if (res.status == 401) {
        this.block.UnBlockUI();
        await this.route.navigateByUrl('login');
        throw res;
        }
        this.block.UnBlockUI();
        if (res.ok) {
        let result = res.json();
        this.ds.Error(result.message || '抱歉,无任何错误信息返回!');
        }
        else {
        if (res.status == 403) {
            this.ds.Error('您没有访问权限!');
        }
        else if (res.status < 200 || res.status > 290) {
            this.ds.Error(res.statusText || '网络异常!');
        }
        }
        throw res;
    }
```

* 使用 


![](../assets/http4.png)

ebvironment.ts  为项目依赖开发测试
ebvironment.prod.ts  为项目线上部署环境

先引入上上步骤建好的 http.server 以及其他服务

```
    import { Injectable } from '@angular/core';
    import { RequestOptionsArgs } from '@angular/http';
    import { HttpService } from '../app/service/http.service';
    import { DomHelperService } from '../app/service/dom-helper.service';
```

声明此服务为API 服务

```
    @Injectable()
    export class ApiService {
    dynamic = true;
    // /BaseService = '/Base';  //基础服务
    // BidService = '/bid';  // 抢单服务
    // DrawService = '/draw';  // 图纸服务
    // DecorationService = '/decoration';   // 装修服务
    // SalaryService = '/salary';     // 结算服务

    // Employee="/employee";//人员管理

    constructor(private http: HttpService,
        private dom: DomHelperService) {
    }
    }
```

接口书写

```
    Account = this.dynamic ? {  // Account 为接口服务名
        LoginBycommon: (option) => this.http.post('/identityserver/account/LoginBycommon', option),
        logout: () => this.http.post('/identityserver/account/logout'),
        revisePwd: (option) => this.http.post(`/Base/api/User/UpdatePasswordByOldPasApp`, option),
        VerifyImage: '/identityserver/account/VerifyImage',
        GetValidCode: () => this.http.get('/identityserver/account/GetValidCode')
    }
        : {
        LoginBycommon: (option) => this.http.get('/assets/json/info.getEmployeeList.json'),
        logout: () => this.http.post('/identityserver/account/logout'),
        revisePwd: (option) => this.http.post(`/Base/api/User/UpdatePasswordByOldPasApp`, option),
        VerifyImage: '/identityserver/account/VerifyImage',
        GetValidCode: () => this.http.get('/identityserver/account/GetValidCode')
        };

```
dynamic 为 true 时  获取线上测试
dynamic 为 false 时  获取前期开发模拟的数据


ebvironment.prod.ts 文件只获取线上数据 

```
     Account = {
        LoginBycommon: (option) => this.http.post('/identityserver/account/LoginBycommon', option),
        logout: () => this.http.post('/identityserver/account/logout'),
        revisePwd: (option) => this.http.post(`/Base/api/User/UpdatePasswordByOldPasApp`, option),
        VerifyImage: '/identityserver/account/VerifyImage',
        GetValidCode: () => this.http.get('/identityserver/account/GetValidCode')
    };
```

组件获取数据

注入 api服务
```
constructor(
              private api: ApiService) {

  }
    // 发送请求
    this.api.ReportDecoration.GetTraceResultList(this.searchOption).then(data => {
      this.groups = data
    });
```