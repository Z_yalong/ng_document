通过 **Component** 装饰器和自定义组件类来创建自定义组件。

`@Component` 装饰器会指出紧随其后的那个类是个组件类，并为其指定元数据。 在下面的范例代码中，你可以看到 `HeroListComponent` 只是一个普通类，完全没有 `Angular` 特有的标记或语法。 直到给它加上了 `@Component` 装饰器，它才变成了组件。

组件的元数据告诉 `Angular` 到哪里获取它需要的主要构造块，以创建和展示这个组件及其视图。 具体来说，它把一个模板（无论是直接内联在代码中还是引用的外部文件）和该组件关联起来。 该组件及其模板，共同描述了一个视图。



```
@Component({
  selector:    'app-hero-list',
  templateUrl: './hero-list.component.html',
  providers:  [ HeroService ]
})
export class HeroListComponent implements OnInit {
/* . . . */
}
```
最常用的 `@Component` 配置选项：

* `selector`：是一个 CSS 选择器，它会告诉 Angular，一旦在模板 HTML 中找到了这个选择器对应的标签，就创建并插入该组件的一个实例。 比如，如果应用的 HTML 中包含 `<app-hero-list></app-hero-list>`，Angular 就会在这些标签中插入一个 `HeroListComponent` 实例的视图。

* `templateUrl`：该组件的 HTML 模板文件相对于这个组件文件的地址。 另外，你还可以用 template 属性的值来提供内联的 HTML 模板。 这个模板定义了该组件的宿主视图。

* `providers`: 是当前组件所需的依赖注入提供商的一个数组。在这个例子中，它告诉 Angular，该组件的构造函数需要一个 `HeroService` 实例，以获取要显示的英雄列表。

### 模板语法

模板很像标准的 `HTML`，但是它还包含` Angular` 的模板语法，这些模板语法可以根据你的应用逻辑、应用状态和 DOM 数据来修改这些 `HTML`。 你的模板可以使用**数据绑定**来协调应用和 `DOM` 中的数据，使用**管道**在显示出来之前对其进行转换，使用**指令**来把程序逻辑应用到要显示的内容上。



```
<h2>Hero List</h2>

<p><i>Pick a hero from the list</i></p>
<ul>
  <li *ngFor="let hero of heroes" (click)="selectHero(hero)">
    {{hero.name}}
  </li>
</ul>

<app-hero-detail *ngIf="selectedHero" [hero]="selectedHero"></app-hero-detail>
```
这个模板使用了典型的 HTML 元素，比如 `<h2>` 和 `<p>`，还包括一些 Angular 的模板语法元素，如 `*ngFor，{{hero.name}}，click、[hero] `和 `<app-hero-detail>`。这些模板语法元素告诉 Angular 该如何根据程序逻辑和数据在屏幕上渲染 HTML。

![](../assets/databinding.png)

* `{{hero.name}}`**插值表达式**在 `<li>` 标签中显示组件的 `hero.name` 属性的值。

* `[hero]`属性绑定把父组件 `HeroListComponent` 的 `selectedHero` 的值传到子组件 `HeroDetailComponent` 的 `hero` 属性中。

* 当用户点击某个英雄的名字时，`(click)` 事件绑定会调用组件的 `selectHero` 方法

* `[(ngModel)]`是 Angular 的双向数据绑定语法。绑定形式是双向数据绑定，它把属性绑定和事件绑定组合成一种单独的写法. 需要从 `@angular/forms` 库中导入 `FormsModule ` ,在双向绑定中，数据属性值通过属性绑定从组件流到输入框。用户的修改通过事件绑定流回组件，把属性值设置为最新的值。

### 管道

Angular 的管道可以让你在模板中声明显示值的转换逻辑。 带有 `@Pipe` 装饰器的类中会定义一个转换函数，用来把输入值转换成供视图显示用的输出值。

Angular 自带了很多管道，比如 `date` 管道和 `currency` 管道，完整的列表参见 [Pipes API](https://angular.cn/api?type=pipe)列表。你也可以自己定义一些新管道。

要在 HTML 模板中指定值的转换方式，请使用 管道操作符 `(|)`。


### 指令

Angular 的模板是动态的。当 Angular 渲染它们的时候，会根据指令给出的指示对 DOM 进行转换。 指令就是一个带有`@Directive` 装饰器的类。

#### 结构型指令

结构型指令通过添加、移除或替换 DOM 元素来修改布局。 这个范例模板使用了两个内置的结构型指令来为要渲染的视图添加程序逻辑：



```
<li *ngFor="let hero of heroes"></li>
<app-hero-detail *ngIf="selectedHero"></app-hero-detail>
```

* `*ngFor` 是一个迭代器，它要求 Angular 为 heroes 列表中的每个 <li> 渲染出一个 <li>。

* `*ngIf` 是个条件语句，只有当选中的英雄存在时，它才会包含 `HeroDetail` 组件。

#### 属性型指令

属性型指令会修改现有元素的外观或行为。 在模板中，它们看起来就像普通的 HTML 属性一样，因此得名“属性型指令”。

Angular 有很多预定义指令，它们或者修改布局结构（比如 `ngSwitch`），或者修改 DOM 元素和组件的某些方面（比如 `ngStyle` 和 `ngClass`）。
