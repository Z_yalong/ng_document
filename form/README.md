### 用户输入

* 通过 `$event` 对象取得用户输入  (不推荐)

```
    <input (keyup)="onKey($event)">
```

* 从一个模板引用变量中获得用户输入

```
    @Component({
        selector: 'app-key-up2',
        template: `
            <input #box (keyup)="onKey(box.value)">
            <p>{{values}}</p>
        `
        })
        export class KeyUpComponent_v2 {
        values = '';
        onKey(value: string) {
            this.values += value + ' | ';
        }
    }
```
* 按键事件过滤（通过 key.enter） 

绑定到 Angular 的 keyup.enter 模拟事件。 然后，只有当用户敲回车键时，Angular 才会调用事件处理器。

```
    <input #box (keyup.enter)="onEnter(box.value)">
```

### 表单

通过 ngModel 跟踪修改状态与有效性验证


   <table>
      <thead>
         <tr>
            <th>状态</th>
            <th>为真时的 CSS 类</th>
            <th>为假时的 CSS 类</th>
         </tr>   
      </thead>
      <tbody>
         <tr>
            <td>控件被访问过。</td>
            <td> ng-touched</td>
            <td> ng-untouched</td>
         </tr>  
         <tr>
            <td>控件的值变化了。</td>
            <td>ng-dirty</td>
            <td>ng-pristine</td>
         </tr>  
         <tr>
            <td>控件的值有效。</td>
            <td>ng-valid</td>
            <td>ng-invalid</td>
         </tr>  
      </tbody>
   </table>

往姓名 `<input>` 标签上添加名叫 `spy` 的临时模板引用变量， 然后用这个 `spy` 来显示它上面的所有 CSS 类。

```
    <input type="text" class="form-control" id="name"
    required
    [(ngModel)]="model.name" name="name"
    #spy>
    <br>表格状态: {{spy.className}}
```
添加css 类

```
    .ng-valid[required], .ng-valid.required  {
    border-left: 5px solid #42A948; /* green */
    }

    .ng-invalid:not(form)  {
    border-left: 5px solid #a94442; /* red */
    }
```
添加错误信息

```
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name"
        required
        [(ngModel)]="model.name" name="name"
        #name="ngModel">
    <div [hidden]="name.valid || name.pristine"
        class="alert alert-danger">
    Name is required
    </div>
```
上例中，当控件是有效的 (`valid`) 或全新的 (`pristine`) 时，隐藏消息。 “全新的”意味着从它被显示在表单中开始，用户还从未修改过它的值。

* 使用 `ngSubmit` 提交该表单

```
    <form (ngSubmit)="onSubmit()" #heroForm="ngForm">
```
你已经定义了一个模板引用变量 `#heroForm`，并且把赋值为“`ngForm`”。 现在，就可以在“Submit”按钮中访问这个表单了。

你要把表单的总体有效性通过 `heroForm` 变量绑定到此按钮的 `disabled` 属性上，代码如下：

```
    <button type="submit" class="btn btn-success" [disabled]="!heroForm.form.valid">Submit</button>
```

### 表单验证

* 模板驱动验证

```
    <input id="name" name="name" class="form-control"
       required minlength="4" appForbiddenName="bob"
       [(ngModel)]="hero.name" #name="ngModel" >

    <div *ngIf="name.invalid && (name.dirty || name.touched)"
        class="alert alert-danger">

    <div *ngIf="name.errors.required">
        Name is required.
    </div>
    <div *ngIf="name.errors.minlength">
        Name must be at least 4 characters long.
    </div>
    <div *ngIf="name.errors.forbiddenName">
        Name cannot be Bob.
    </div>

    </div>
```
请注意以下几点：

1. `<input>` 元素带有一些 HTML 验证属性：required 和 minlength。它还带有一个自定义的验证器指令 forbiddenName。要了解更多信息，参见自定义验证器一节。

2. `#name="ngModel"` 把 `NgModel` 导出成了一个名叫 name 的局部变量。NgModel 把自己控制的 `FormControl` 实例的属性映射出去，让你能在模板中检查控件的状态，比如 `valid` 和 `dirty`。要了解完整的控件属性，参见 API 参考手册中的 `AbstractControl。`

3. `<div> `元素的 `*ngIf` 揭露了一套嵌套消息 `divs`，但是只在有“name”错误和控制器为 `dirty` 或者 `touched。`

4. 每个嵌套的 `<div>` 为其中一个可能出现的验证错误显示一条自定义消息。比如 `required、minlength` 和 `forbiddenName。`

5. 响应式表单的验证

在响应式表单中，真正的源码都在组件类中。不应该通过模板上的属性来添加验证器，而应该在组件类中直接把验证器函数添加到表单控件模型上`（FormControl）`。然后，一旦控件发生了变化，Angular 就会调用这些函数。

* 验证器函数

有两种验证器函数：同步验证器和异步验证器。

 1.同步验证器函数接受一个控件实例，然后返回一组验证错误或 null。你可以在实例化一个 FormControl 时把它作为构造函数的第二个参数传进去。

 2.异步验证器函数接受一个控件实例，并返回一个承诺（Promise）或可观察对象（Observable），它们稍后会发出一组验证错误或者 null。你可以在实例化一个 FormControl 时把它作为构造函数的第三个参数传进去。

 3.注意：出于性能方面的考虑，只有在所有同步验证器都通过之后，Angular 才会运行异步验证器。当每一个异步验证器都执行完之后，才会设置这些验证错误。

* 内置验证器

模板驱动表单中可用的那些属性型验证器（如 required、minlength 等）对应于 Validators 类中的同名函数。要想查看内置验证器的全列表，参见 [API](https://angular.cn/api/forms/Validators) 参考手册中的验证器部分。

```
    ngOnInit(): void {
    this.heroForm = new FormGroup({
        'name': new FormControl(this.hero.name, [
        Validators.required,
        Validators.minLength(4)
        ]),
        'alterEgo': new FormControl(this.hero.alterEgo),
        'power': new FormControl(this.hero.power, Validators.required)
    });
    }

    get name() { return this.heroForm.get('name'); }

    get power() { return this.heroForm.get('power'); }
```

### 自定义验证器

由于内置验证器无法适用于所有应用场景，有时候你还是得创建自定义验证器。

建立一个 `forbiddenNameValidator` 函数。该函数的定义看起来是这样的：

```
    /** A hero's name can't match the given regular expression */
    export function forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const forbidden = nameRe.test(control.value);
        return forbidden ? {'forbiddenName': {value: control.value}} : null;
    };
    }
```
添加到验证里面

```
    this.heroForm = new FormGroup({
    'name': new FormControl(this.hero.name, [
        Validators.required,
        Validators.minLength(4),
        forbiddenNameValidator(/bob/i) // <-- Here's how you pass in the custom validator.
    ]),
    'alterEgo': new FormControl(this.hero.alterEgo),
    'power': new FormControl(this.hero.power, Validators.required)
    });
```
### 响应式表单

 * 创建模板

```
    <input class="form-control" [formControl]="name">
```

* 导入 `ReactiveFormsModule`

```
    import { NgModule }            from '@angular/core';
    import { BrowserModule }       from '@angular/platform-browser';
    import { ReactiveFormsModule } from '@angular/forms';  // <-- #1 import module

    import { AppComponent }        from './app.component';
    import { HeroDetailComponent } from './hero-detail/hero-detail.component';

    @NgModule({
    declarations: [
        AppComponent,
        HeroDetailComponent,
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule // <-- #2 add to @NgModule imports
    ],
    bootstrap: [ AppComponent ]
    })
    export class AppModule { }
```

* 基础的表单类


   <table>
      <thead>
         <tr>
            <th>类</th>
            <th>说明</th>
         </tr>   
      </thead>
      <tbody>
         <tr>
            <td>AbstractControl</td>
            <td>AbstractControl是这三个具体表单类的抽象基类。 并为它们提供了一些共同的行为和属性。</td>
         </tr>  
         <tr>
            <td>FormControl</td>
            <td>FormControl 用于跟踪一个单独的表单控件的值和有效性状态。它对应于一个 HTML 表单控件，比如 input 或 select。</td>
         </tr>  
         <tr>
            <td>FormGroup</td>
            <td>FormGroup用于 跟踪一组AbstractControl 的实例的值和有效性状态。 该组的属性中包含了它的子控件。 组件中的顶级表单就是一个 FormGroup</td>
         </tr>   
         <tr>
            <td>FormArray</td>
            <td>FormArray用于跟踪 AbstractControl 实例组成的有序数组的值和有效性状态。</td>
         </tr>  
      </tbody>
   </table>

* 添加 `FormGroup`

```
    import { Component }              from '@angular/core';
    import { FormControl, FormGroup } from '@angular/forms';
```

在这个类中，把 `FormControl` 包裹进了一个名叫 `heroForm` 的 `FormGroup` 中，代码如下：

```
    export class HeroDetailComponent2 {
    heroForm = new FormGroup ({
        name: new FormControl()
    });
    }
```
`heroForm.value` 获取值

现在你改完了这个类，该把它映射到模板中了。改成这样：

```
    <form [formGroup]="heroForm">
    <div class="form-group">
        <label class="center-block">Name:
        <input class="form-control" formControlName="name">
        </label>
    </div>
    </form>
```

* `FormBuilder` 简介

FormBuilder 类能通过处理控件创建的细节问题来帮你减少重复劳动。

要使用 FormBuilder，就要先把它导入

```
    import { FormBuilder, FormGroup } from '@angular/forms';
```

修改过的 `HeroDetailComponent` 代码如下：

```
    export class HeroDetailComponent3 {
    heroForm: FormGroup; // <--- heroForm is of type FormGroup

    constructor(private fb: FormBuilder) { // <--- inject FormBuilder
        this.createForm();
    }

    createForm() {
        this.heroForm = this.fb.group({
        name: '', // <--- the FormControl called "name"
        });
    }
    }
```

* `Validators.required`

首先，导入 Validators 符号。

```
    import { FormBuilder, FormGroup, Validators } from '@angular/forms';

    this.heroForm = this.fb.group({
    name: ['', Validators.required ],
    street: '',
      city: '',
      state: '',
      zip: '',
      power: '',
      sidekick: ''
    });
```
要想让 `name` 这个 `FormControl` 是必须的，请把 `FormGroup` 中的 `name` 属性改为一个数组。第一个条目是 `name` 的初始值，第二个是 `required` 验证器：`Validators.required`。

* 查看 `FormControl` 的属性

你可以使用 `.get()` 方法来提取表单中一个单独 `FormControl` 的状态

heroForm.get('name').value


* 使用 `setValue()` 和 `patchValue()` 来操纵表单模型

借助`setValue()`，你可以设置每个表单控件的值，只要把与表单模型的属性精确匹配的数据模型传进去就可以了。

```
    this.heroForm.setValue({
    name:    this.hero.name,
    address: this.hero.addresses[0] || new Address()
    });
```

借助`patchValue()`，你可以通过提供一个只包含要更新的控件的键值对象来把值赋给 FormGroup 中的指定控件。

```
    this.heroForm.patchValue({
    name: this.hero.name
    });
```
借助`patchValue()`，你可以更灵活地解决数据模型和表单模型之间的差异。 但是和` setValue()` 不同，`patchValue()` 不会检查缺失的控件值，并且不会抛出有用的错误信息。

* 使用 FormArray 来表示 FormGroup 数组

`FormGroup` 是一个命名对象，它的属性值是 `FormControl` 和其它的 `FormGroup。`

有时你需要表示任意数量的控件或控件组。 比如，一个英雄可能拥有 0、1 或任意数量的住址。

要访问 FormArray 类，请先把它导入

```
    import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
```

要使用 `FormArray`，就要这么做：

1. 在数组中定义条目 `FormControl` 或 `FormGroup。`

2. 把这个数组初始化微一组从数据模型中的数据创建的条目。

3. 根据用户的需求添加或移除这些条目。

把 `FormGroup` 型的住址替换为 `FormArray` 型的 `secretLairs` 定义：

```
    this.heroForm = this.fb.group({
    name: ['', Validators.required ],
    secretLairs: this.fb.array([]), // <-- secretLairs as an empty FormArray
    power: '',
    sidekick: ''
    });   
```

下面的 `setAddresses()` 方法把 `secretLairs` 这个 `FormArray` 替换为一个新的 `FormArray`，使用一组表示英雄地址的 `FormGroup` 来进行初始化。在 `HeroDetailComponent` 类中添加下列内容：

```
    setAddresses(addresses: Address[]) {
    const addressFGs = addresses.map(address => this.fb.group(address));
    const addressFormArray = this.fb.array(addressFGs);
    this.heroForm.setControl('secretLairs', addressFormArray);
    }
```
注意，你使用 `FormGroup.setControl()` 方法，而不是 `setValue()` 方法来替换前一个 `FormArray。` 你所要替换的是控件，而不是控件的值。

* 获取 `FormArray`

使用 `FormGroup.get()` 方法来获取到 `FormArray` 的引用。 把这个表达式包装进一个名叫 `secretLairs` 的便捷属性中来让它更清晰，并供复用。 在 `HeroDetailComponent` 中添加下列内容。

```
   get secretLairs(): FormArray {
    return this.heroForm.get('secretLairs') as FormArray;
    }; 
```
* 显示 FormArray

```
    <div formArrayName="secretLairs" class="well well-lg">
    <div *ngFor="let address of secretLairs.controls; let i=index" [formGroupName]="i" >
        <!-- The repeated address template -->
    </div>
    </div>
```