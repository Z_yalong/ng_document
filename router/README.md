### 路由与导航 

在用户使用应用程序时，Angular 的路由器能让用户从一个视图导航到另一个视图。


* 从路由库中导入

```
    import { RouterModule, Routes } from '@angular/router';
```

* 配置
 
 每个带路由的 Angular 应用都有一个Router（路由器）服务的单例对象。 当浏览器的 URL 变化时，路由器会查找对应的 Route（路由），并据此决定该显示哪个组件。

 ```
    const appRoutes: Routes = [  
    { path: 'crisis-center', component: CrisisListComponent },  
     // path 不能以斜杠（/）开头。
    { path: 'hero/:id',      component: HeroDetailComponent },   
    // 第二个路由中的 :id 是一个路由参数的令牌(Token)。比如 /hero/42 这个 URL 中，“42”就是 id 参数的值。 此 URL 对应的 HeroDetailComponent 组件将据此查找和展现 id 为 42 的英雄。
    {
        path: 'heroes',   
        component: HeroListComponent,
        data: { title: 'Heroes List' }   
        // 第三个路由中的 data 属性用来存放于每个具体路由有关的任意信息。该数据可以被任何一个激活路由访问，并能用来保存诸如 页标题、面包屑以及其它静态只读数据。
    },
    { path: '',  
    // 第四个路由中的空路径（''）表示应用的默认路径，当 URL 为空时就会访问那里，因此它通常会作为起点。 这个默认路由会重定向到 URL /heroes，并显示 HeroesListComponent。
        redirectTo: '/heroes',
        pathMatch: 'full'
    },
    { path: '**', component: PageNotFoundComponent }  
    // 最后一个路由中的 ** 路径是一个通配符。当所请求的 URL 不匹配前面定义的路由表中的任何路径时，路由器就会选择此路由。 这个特性可用于显示“404 - Not Found”页，或自动重定向到其它路由。
    ];

    @NgModule({
    imports: [
        RouterModule.forRoot(  //  把它传给 RouterModule.forRoot 方法并传给本模块的 imports 数组就可以配置路由器。
        appRoutes,
        { enableTracing: true } // <-- debugging purposes only
        )
        // other imports here
    ],
    ...
    })
    export class AppModule { }
 ```

* 路由出口

```
    <router-outlet></router-outlet>
```
* 路由器链接

    1. routerLink    `<a routerLink="/crisis-center" >Crisis Center</a>`

    ```
     constructor(
        private activeRouter: ActivatedRoute,
        private router: Router,
        private service: HeroService
        ) {}
    ```
    2. this.router.navigate(['crisis-center', cucId], {relativeTo: this.activeRouter.parent.parent}); 
       
    ` <a routerLink="/crisis-center" routerLinkActive="active">Crisis Center</a>`
    每个 a 标签上的`RouterLinkActive`指令可以帮用户在外观上区分出当前选中的“活动”路由。 当与它关联的 RouterLink 被激活时，路由器会把 CSS 类 `active` 添加到这个元素上。 你可以把该指令添加到 a 元素或它的父元素上。

* 路由器状态

在导航时的每个生命周期成功完成时，路由器会构建出一个 `ActivatedRoute` 组成的树，它表示路由器的当前状态。 你可以在应用中的任何地方用 `Router` 服务及其 `routerState` 属性来访问当前的 `RouterState` 值。

`RouterState` 中的每个 `ActivatedRoute` 都提供了从任意激活路由开始向上或向下遍历路由树的一种方式，以获得关于父、子、兄弟路由的信息。

`ActivatedRoute` 的值

 <table>
      <thead>
         <tr>
            <th>属性</th>
            <th>说明</th>
         </tr>   
      </thead>
      <tbody>
         <tr>
            <td>url</td>
            <td> 路由路径的 Observable 对象，是一个由路由路径中的各个部分组成的字符串数组。</td>
         </tr>  
         <tr>
            <td>data</td>
            <td> 一个 Observable，其中包含提供给路由的 data 对象。也包含由解析守卫（resolve guard）解析而来的值。</td>
         </tr>  
         <tr>
            <td>paramMap</td>
            <td> 一个 Observable，其中包含一个由当前路由的必要参数和可选参数组成的map对象。用这个 map 可以获取来自同名参数的单一值或多重值。</td>
         </tr>   
         <tr>
            <td>queryParamMap</td>
            <td> 一个 Observable，其中包含一个对所有路由都有效的查询参数组成的map对象。 用这个 map 可以获取来自查询参数的单一值或多重值。</td>
         </tr>  
         <tr>
            <td>fragment</td>
            <td> 一个适用于所有路由的 URL 的 fragment（片段）的 Observable。</td>
         </tr>  
         <tr>
            <td>outlet</td>
            <td> 要把该路由渲染到的 RouterOutlet 的名字。对于无名路由，它的路由名是 primary，而不是空串。</td>
         </tr>  
         <tr>
            <td>routeConfig</td>
            <td> 用于该路由的路由配置信息，其中包含原始路径。</td>
         </tr>  
         <tr>
            <td>parent</td>
            <td>当该路由是一个子路由时，表示该路由的父级 ActivatedRoute。</td>
         </tr>  
         <tr>
            <td>firstChild</td>
            <td>包含该路由的子路由列表中的第一个 ActivatedRoute。</td>
         </tr>  
         <tr>
            <td>children</td>
            <td>包含当前路由下所有已激活的子路由。</td>
         </tr>  
      </tbody>
   </table>

* 路由事件

<table>
      <thead>
         <tr>
            <th>路由器事件</th>
            <th>说明</th>
         </tr>   
      </thead>
      <tbody>
         <tr>
            <td>NavigationStart</td>
            <td> 本事件会在导航开始时触发。</td>
         </tr>  
         <tr>
            <td>RoutesRecognized</td>
            <td>本事件会在路由器解析完 URL，并识别出了相应的路由时触发</td>
         </tr>  
         <tr>
            <td>RouteConfigLoadStart</td>
            <td> 本事件会在 Router 对一个路由配置进行惰性加载之前触发。</td>
         </tr>   
         <tr>
            <td>RouteConfigLoadEnd</td>
            <td>本事件会在路由被惰性加载之后触发。</td>
         </tr>  
         <tr>
            <td>NavigationEnd</td>
            <td>本事件会在导航成功结束之后触发。</td>
         </tr>  
         <tr>
            <td>NavigationCancel</td>
            <td>本事件会在导航被取消之后触发。 这可能是因为在导航期间某个路由守卫返回了 false。</td>
         </tr>  
         <tr>
            <td>NavigationError</td>
            <td>这个事件会在导航由于意料之外的错误而失败时触发。</td>
         </tr>  
      </tbody>
   </table>

   当打开了 `enableTracing` 选项时，这些事件也同时会记录到控制台中。由于这些事件是以 `Observable` 的形式提供的，所以你可以对自己感兴趣的事件进行 `filter()`，并 `subscribe()` 它们，以便根据导航过程中的事件顺序做出决策。

   该应用有一个配置过的路由器。 外壳组件中有一个` RouterOutlet`，它能显示路由器所生成的视图。 它还有一些 `RouterLink`，用户可以点击它们，来通过路由器进行导航。

   下面是一些路由器中的关键词汇及其含义：

   <table>
      <thead>
         <tr>
            <th>路由器部件</th>
            <th>含义</th>
         </tr>   
      </thead>
      <tbody>
         <tr>
            <td>Router（路由器）</td>
            <td> 为激活的 URL 显示应用组件。管理从一个组件到另一个组件的导航</td>
         </tr>  
         <tr>
            <td>RouterModule</td>
            <td>一个独立的 Angular 模块，用于提供所需的服务提供商，以及用来在应用视图之间进行导航的指令。</td>
         </tr>  
         <tr>
            <td>Routes（路由数组）</td>
            <td>定义了一个路由数组，每一个都会把一个 URL 路径映射到一个组件。</td>
         </tr>   
         <tr>
            <td>Route（路由）</td>
            <td>定义路由器该如何根据 URL 模式（pattern）来导航到组件。大多数路由都由路径和组件类构成。</td>
         </tr>  
         <tr>
            <td>RouterOutlet（路由出口）</td>
            <td>该指令（router-outlet）用来标记出路由器该在哪里显示视图。</td>
         </tr>  
         <tr>
            <td>RouterLink（路由链接）</td>
            <td>这个指令把可点击的 HTML 元素绑定到某个路由。点击带有 routerLink 指令（绑定到字符串或链接参数数组）的元素时就会触发一次导航。</td>
         </tr>  
         <tr>
            <td>RouterLinkActive（活动路由链接）</td>
            <td>当 HTML 元素上或元素内的routerLink变为激活或非激活状态时，该指令为这个 HTML 元素添加或移除 CSS 类。</td>
         </tr>  
         <tr>
            <td>ActivatedRoute（激活的路由）</td>
            <td>为每个路由组件提供提供的一个服务，它包含特定于路由的信息，比如路由参数、静态数据、解析数据、全局查询参数和全局碎片（fragment）。</td>
         </tr>  
         <tr>
            <td>RouterState（路由器状态）</td>
            <td>路由器的当前状态包含了一棵由程序中激活的路由构成的树。它包含一些用于遍历路由树的快捷方法。</td>
         </tr>  
         <tr>
            <td>链接参数数组</td>
            <td>这个数组会被路由器解释成一个路由操作指南。你可以把一个RouterLink绑定到该数组，或者把它作为参数传给Router.navigate方法。路由器的当前状态包含了一棵由程序中激活的路由构成的树。它包含一些用于遍历路由树的快捷方法。</td>
         </tr>  
         <tr>
            <td>路由组件</td>
            <td>一个带有RouterOutlet的 Angular 组件，它根据路由器的导航来显示相应的视图。</td>
         </tr>  
      </tbody>
   </table>

* 带参数的路由的参数获取

```
    constructor(
    private activeRouter: ActivatedRoute,
    private router: Router,
    private service: HeroService
    ) {}
    ngOnInit() {
        this.activeRouter.params.subscribe(params => {
        if (params['cucId']) {
            this.cucId = params['cucId'];
        }
    }
```

* 路由守卫

路由器可以支持多种守卫接口：

* 用`CanActivate`来处理导航到某路由的情况。

* 用`CanActivateChild`来处理导航到某子路由的情况。

* 用`CanDeactivate`来处理从当前路由离开的情况.

* 用`Resolve`在路由激活之前获取路由数据。

* 用`CanLoad`来处理异步导航到某特性模块的情况。

这是一种具有通用性的守护目标（通常会有其它特性需要登录用户才能访问），所以你要在应用的根目录下创建一个 `auth-guard.ts` 文件。

```
    import { Injectable }     from '@angular/core';
    import { CanActivate }    from '@angular/router';

    @Injectable()
    export class AuthGuard implements CanActivate {
    canActivate() {
        console.log('AuthGuard#canActivate called');
        return true;
    }
    }
```
接下来，导入 `AuthGuard `类，修改管理路由并通过 `CanActivate()` 守卫来引用 `AuthGuard`：  `CanActivateChild`：保护子路由 (同理)

```
    import { AuthGuard }                from '../auth-guard.service';

    const adminRoutes: Routes = [
    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard],
        children: [
        {
            path: '',
            children: [
            { path: 'crises', component: ManageCrisesComponent },
            { path: 'heroes', component: ManageHeroesComponent },
            { path: '', component: AdminDashboardComponent }
            ],
        }
        ]
    }
    ];
```

惰性加载路由

给它一个 `loadChildren` 属性（注意不是 `children` 属性），把它设置为 `AdminModule` 的地址。

```
    {
    path: 'admin',
    loadChildren: 'app/admin/admin.module#AdminModule',
    },
```