工程结构图如下：
    ![](../assets/3020614-f8532e8060b7aab9.png)

    e2e 端到端的测试目录 用来做自动测试的

    node_modules 第三方依赖包存放目录

    src 应用源代码目录

    app目录               包含应用的组件和模块，我们要写的代码都在这个目录

    app.component.ts— 组件的类代码，这是用 TypeScript 写的。

    app.component.html— 组件的模板，这是用 HTML 写的。

    app.component.css— 组件的私有 CSS 样式。

    app.component.spec.ts— 组件的单元测试。


    assets目录            资源目录，存储静态资源的
    比如图片

    environments目录      环境配置。Angular是支持多环境开发的，我们可以在不同的环境下（开发环境，测试环境，生产环境）共用一套代码，主要用来配置环境的

    index.html          整个应用的根html，程序启动就是访问这个页面

    main.ts             整个项目的入口点，Angular通过这个文件来启动项目

    polyfills.ts        主要是用来导入一些必要库，为了让Angular能正常运行在老版本下
    styles.css          主要是放一些全局的样式

    tsconfig.app.json   TypeScript编译器的配置,添加第三方依赖的时候会修改这个文件

    tsconfig.spec.json  不用管

    test.ts             也是自动化测试用的

    typings.d.ts        不用管



    angular-cli.json Angular命令行工具的配置文件。后期可能会去修改它，引一些其他的第三方的包 比如jquery等

    karma.conf.js karma是单元测试的执行器，karma.conf.js是karma的配置文件

    package.json 这是一个标准的npm工具的配置文件，这个文件里面列出了该应用程序所使用的第三方依赖包。实际上我们在新建项目的时候，等了半天就是在下载第三方依赖包。下载完成后会放在node_modules这个目录中，后期我们可能会修改这个文件。

    protractor.conf.js 也是一个做自动化测试的配置文件

    README.md 说明文件

    tslint.json 是tslint的配置文件，用来定义TypeScript代码质量检查的规则

### 常用指令

创建module，component，service，class

![](../assets/3020614-243cbfb9a17f3043.png)

<p>列 ：创建一个home的module；</p>
 `ng g component home`

![](../assets/3020614-0deaed202b49d980.png)

app下面多了一个home的component，它应有的css，html，*.spec.ts(测试代码)，ts都生成了；css，html，ts，*.spec.ts都放在这下面,方便组件化可以重复使用，提高效率，庞大的工程代码中可以提高可维护性，阅读性，方便测试等等


再试着创建一个angular的模块
`ng g module about`

![](../assets/3020614-41953f0517ab924d.png)

模块比component多了一个module.ts文件
