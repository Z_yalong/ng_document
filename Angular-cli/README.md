### AngularCLI主要特性

1. Angular CLI 可以快速搭建框架，创建module，service，class，directive等；

2. 具有webpack的功能，代码分割（code splitting），按需加载；

3. 代码打包压缩；

4. 模块测试，端到端测试；

5. 热部署，有改动立即重新编译，不用刷新浏览器；而且速度很快

6. 有开发环境，测试环境，生产环境的配置，不用自己操心；

7. sass，less的预编译Angular CLI都会自动识别后缀来编译；

8. typescript的配置，Angular CLI在创建应用时都可以自己配置；

9. 在创建好的工程也可以做一些个性化的配置，webpack的具体配置还不支持，未来可能会增加；

10. Angular CLI创建的工程结构是最佳实践，生产可用；

